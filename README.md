# system commands

## USB DRIVE

pour lister les disques
'fdisk - l'

pour monter le disque
'mount /dev/sdb1 /mnt

pour trouver les disques par UID
' ls -l /dev/disk/by-uuid/*'

pour monter définitivement un disque
'nano /etc/fstab'
'/dev/sdb1 /mnt vfat dmask=000,fmask=0111,user 0 0'

monter tous les disques de fstab
'mount -a'

démonter un disque
'unmount /dev/sbd1'

pour installer notamment les outils de formatage
'apt-get install dosfstools'

pour formater en fat32
'mkfs.vfat /dev/sdb1'

pour lister les espaces et les disques
'df'

## SAMBA

Fichier de conf
'/etc/samba/smb.conf'

Service samba
' service smbd stop'

[usb]
path = /medias/usb-drive
valid users = partage
read only = no




